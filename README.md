# A simple tcp server from scratch

## Client 連線失敗情況

- 網路不通，服務器未開啟
- 服務器的`listen backlog`滿了
- 網路延遲，導致連線超時

## Socket 讀操作

- `Go runtime`幫助用戶隱藏了`I/O多路復用`的複雜性
- 用戶只需要採用`Goroutine`+一般`阻塞I/O模型`方式，即可滿足大部分需求
- 使用`net.Conn`接口，底層類型為`*TCPConn`，`conn`封裝了底層`socket`
```go

//$GOROOT/src/net/tcpsock.go
type TCPConn struct {
    conn
}
//$GOROOT/src/net/net.go
type conn struct {
    fd *netFD
}
```

- 從`Socket`讀取數據行為
  - 無數據
    - 這個跟前面說的`blocking I/O`是一致的。因為client未發送數據，`Go runtime`會把`goroutine`阻塞，等待有數據時候，再重新調度這個`socket`對應的`Goroutine`
  - 部分數據
    - 只要有數據就緒，且數據長度小於一次讀取操作長度，即會直接返回讀取操作
  - 足夠數據
    - 數據就緒，且數據長度大於等於一次讀取長度，即直接返回一次讀取長度的數據
  - 設置讀取操作超時
    - 通過`net.Conn`的`SetReadDeadline`方法，設置讀取操作的時間，當超時發生，`Read`操作會解除阻塞並返回超時錯誤
    - 一旦使用`SetReadDeadline`方法設置後，當超時後，如果不進行重新設置`Deadline`，後來與這個`socket`有關讀取操作都會是超時失敗

## Socket 寫操作

- 寫入數據，特殊情況
  - 寫阻塞
    - `TCP`協議通訊雙方OS內核，都會為這個連結保留數據緩衝區。調用Wrtie方法向socket寫入數據，實際上是把數據寫入到OS的緩衝區，待溢出即阻塞
  - 寫入部分數據
    - 當伺服器斷線或是出現問題，導致無法寫入就會收到`broken pipe`錯誤
    - 如`demo`的`client`示意圖
  - 寫入超時
    - 跟讀取操作一樣，有個寫入超時操作，調用`SetWriteDeadine`方法，如果後續沒有取消重置，只要超時都會對該socket寫入操作以超時失敗告終

## 併發 socket 讀寫

- 讀操作
  - 因`TCP`是`byte`切片，`conn.Read`無法正確區分業務數據的邊界，所以多個`Goroutine`對同一個`conn`影響不大，反而是`Goroutine`讀到不完整的業務package，會增加處理業務難度
- 寫操作
  - `net.Conn`只是`*netFD`的外層結構體

```go
    //$GOROOT/src/net/net.go
    type conn struct {
        fd *netFD
    }
    
    // $GOROOT/src/net/fd_posix.go
    
    // Network file descriptor.
    type netFD struct {
    pfd poll.FD
    
    // immutable until Close
    family      int
    sotype      int
    isConnected bool // handshake completed or use of association with peer
    net         string
    laddr       Addr
    raddr       Addr
    }
```
  - 在源碼中，FD類型中包含了`runtime`實現的`fdMutex`類型。從源碼注釋來看，所有對這個`FD`所代表的連結的`Read`和`Write`操作，都是由`fdMutex`來同步的

## demo 


  - server
  ![img.png](assets/server1.png)
  ![img.png](assets/server2.png)
  - client
  ![img.png](assets/client1.png)
  ![img.png](assets/client2.png)
