package main

import (
	"log"
	"net"
	"time"
)

func main() {

	//conn, err := net.Dial("tcp", "localhost:8080")
	conn, err := net.DialTimeout("tcp", "localhost:8080", time.Second*3)
	//defer conn.Close()
	if err != nil {
		log.Fatal("Connect fail,", err)
		return
	}
	log.Println("log, Connect success")
	defer conn.Close()

	// write data
	data := make([]byte, 65536)
	var total int
	for {
		n, err := conn.Write(data)
		if err != nil {
			total += n
			log.Fatalf("write %d bytes data, error: %s\n", n, err)
			break
		}
		total += n
		log.Printf("write %d bytes this time, %d bytes in total\n", n, total)
	}
	log.Printf("write %d bytes in total\n", total)
}
