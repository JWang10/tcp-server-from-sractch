package main

import (
	"fmt"
	"log"
	"net"
	"time"
)

func main() {
	fmt.Println("TCP-Server")
	conn, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Printf("listen error: %s\n", err)
		return
	}
	for {
		c, err := conn.Accept()
		if err != nil {
			fmt.Printf("accept error:%s\n", err)
			break
		}
		go handleConn(c) // new goroutine to handle the connection
	}
	time.Sleep(time.Second * 10)
}

func handleConn(c net.Conn) {
	defer c.Close()
	time.Sleep(time.Second * 5)
	for {
		time.Sleep(time.Second * 5)
		var buff = make([]byte, 60000)
		log.Println("start to read data from socket")
		n, err := c.Read(buff)
		if err != nil {
			log.Printf("conn read %d bytes, error: %s\n", n, err)
			if nerr, ok := err.(net.Error); ok && nerr.Timeout() {
				continue
			}
		}
		log.Printf("read %d bytes, content: %s\n", n, string(buff[:n]))
	}
}
